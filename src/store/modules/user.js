import Vue from 'vue'
import { login, getInfo, logout } from '@/api/login'
import { ACCESS_TOKEN } from '@/store/mutation-types'
import { welcome } from '@/utils/util'

const user = {
  state: {
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {}
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        login(userInfo).then(response => {
          const result = response.data
          console.log('login action',result)
          Vue.ls.set(ACCESS_TOKEN, result.token, 7 * 24 * 60 * 60 * 1000)
          commit('SET_TOKEN', result.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },
    
    // 获取用户信息
    GetInfo ({ commit }) {
      
      const result = {
        'id': '4291d7da9005377ec9aec4a71ea837f',
        'name': '管理员',
        'username': 'admin',
        'password': '',
        'avatar': '/avatar2.jpg',
        'status': 1,
        'telephone': '',
        'lastLoginIp': '27.154.74.117',
        'lastLoginTime': 1534837621348,
        'creatorId': 'admin',
        'createTime': 1497160610259,
        'merchantCode': 'TLif2btpzg079h15bk',
        'deleted': 0,
        'roleId': 'admin',
        'role': {}
      }
      commit('SET_INFO', result)
      commit('SET_NAME', { name: result.name, welcome: welcome() })
      commit('SET_AVATAR', result.avatar)
      // return new Promise((resolve, reject) => {
        
      //   getInfo().then(response => {
      //     const result = response.result

      //     // if (result.role && result.role.permissions.length > 0) {
      //     //   const role = result.role
      //     //   role.permissions = result.role.permissions
      //     //   role.permissions.map(per => {
      //     //     if (per.actionEntitySet != null && per.actionEntitySet.length > 0) {
      //     //       const action = per.actionEntitySet.map(action => { return action.action })
      //     //       per.actionList = action
      //     //     }
      //     //   })
      //     //   role.permissionList = role.permissions.map(permission => { return permission.permissionId })
      //     //   commit('SET_ROLES', result.role)
      //     //   commit('SET_INFO', result)
      //     // } else {
      //     //   reject(new Error('getInfo: roles must be a non-null array !'))
      //     // }
      //     commit('SET_INFO', result)
      //     commit('SET_NAME', { name: result.name, welcome: welcome() })
      //     commit('SET_AVATAR', result.avatar)

      //     resolve(response)
      //   }).catch(error => {
      //     reject(error)
      //   })
      // })
    },

    // 登出
    Logout ({ commit, state }) {
      return new Promise((resolve) => {
        logout(state.token).then(() => {
          resolve()
        }).catch(() => {
          resolve()
        }).finally(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          Vue.ls.remove(ACCESS_TOKEN)
        })
      })
    }
  }
}

export default user
