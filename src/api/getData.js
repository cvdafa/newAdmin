import api from './index'
import { axios } from '@/utils/request'

export function taskRelease (task_id, task_url, task_progress, task_integral, task_release, task_ok_time, task_type, task_note, url_check) {
  return axios({
    url: '/v1/admin/task/release',
    method: 'post',
    data: {
      task_id, task_url, task_progress, task_integral, task_release, task_ok_time, task_type, task_note, url_check
    }
  })
}
export function getTaskList (taskId,taskType,page,pageSize,search,sortKey,sort) {
  return axios({
    url: `v1/admin/task/taskList?taskId=${taskId}&taskType=${taskType}&page=${page}&pageSize=${pageSize}&search=${search}&sortKey=${sortKey}&sort=${sort}`,
    method: 'get'
  })
}

export function queryTask (page,pageSize,search,status,taskId,taskType) {
  return axios({
    url: `v1/admin/task/record?page=${page}&pageSize=${pageSize}&search=${search}&status=${status}&taskId=${taskId}&taskType=${taskType}`,
    method: 'get'
  })
}

export function taskAudit (id,audit_status,audit_note) {
  return axios({
    url: `v1/admin/task/audit?id=${id}&audit_status=${audit_status}&audit_note=${audit_note}`,
    method: 'get'
  })
}

export function getIntegralList (page,pageSize,search) {
  return axios({
    url: `v1/admin/integral/list?page=${page}&pageSize=${pageSize}&search=${search}`,
    method: 'get'
  })
}

export function setTaskTop (id,top) {
  return axios({
    url: `v1/admin/task/top?taskListId=${id}&top=${top}`,
    method: 'get'
  })
}

export function outList (page,pageSize,search,status) {
  return axios({
    url: `v1/admin/out/list?page=${page}&pageSize=${pageSize}&search=${search}&status=${status}`,
    method: 'get'
  })
}

export function outMod (outId,status,note) {
  return axios({
    url: `v1/admin/out/mod`,
    method: 'post',
    data: {
      outId,status,note
    }
  })
}

export function feedList (page,pageSize,search,status) {
  return axios({
    url: `v1/admin/feedback/list?page=${page}&pageSize=${pageSize}&search=${search}&status=${status}`,
    method: 'get'
  })
}

export function setfeedback (fd_id,note) {
  return axios({
    url: `v1/admin/feedback/mod`,
    method: 'post',
    data: {
      fd_id,
      note
    }
  })
}

export function problem (page,pageSize) {
  return axios({
    url: `v1/admin/problem/list?page=${page}&pageSize=${pageSize}`,
    method: 'get'
  })
}

export function addProblem(title,content,sort) {
  return axios({
    url: `v1/admin/problem/add`,
    method: 'post',
    data: {
      title,content,sort
    }
  })
}

export function upProblem(id,title,content,sort) {
  return axios({
    url: `v1/admin/problem/mod`,
    method: 'post',
    data: {
      id,title,content,sort
    }
  })
}

export function enableProblem(id,enable) {
  return axios({
    url: `v1/admin/problem/enable`,
    method: 'post',
    data: {
      id,enable
    }
  })
}

export function delProblem (id) {
  return axios({
    url: `v1/admin/problem/delete?id=${id}`,
    method: 'get'
  })
}
export function configList () {
  return axios({
    url: `v1/admin/config/lists`,
    method: 'get'
  })
}
export function upconfig(name,value) {
  return axios({
    url: `v1/admin/config/mod?key_name=${name}&key_value=${value}`,
    method: 'get'
  })
}

export function deleteUser(userId) {
  return axios({
    url: `v1/admin/member/delete`,
    method: 'post',
    data: {
      userId
    }
  })
}

export function updataUser(userId,password,zfb_user,zfb_name,agent_id,agent_fee) {
  return axios({
    url: `v1/admin/member/mod`,
    method: 'post',
    data: {
      userId,password,zfb_user,zfb_name,agent_id,agent_fee
    }
  })
}
export function taskLists (split='') {
  return axios({
    url: `v1/admin/task/lists?isSplit=${split}`,
    method: 'get'
  })
}
export function taskCreate(object) {
  return axios({
    url: `v1/admin/task/create`,
    method: 'post',
    data: object
  })
}

export function taskEdite(object) {
  return axios({
    url: `v1/admin/task/edit`,
    method: 'post',
    data: object
  })
}

export function taskListEdit(taskListId,task_progress,task_status,task_integral) {
  let datas = {
    taskListId,
    task_integral
  }
  if(task_progress!==''){
    datas.task_progress = task_progress
  }
  if(task_status!==''){
    datas.task_status = task_status
  }
  return axios({
    url: `v1/admin/task/listEdit`,
    method: 'post',
    data: datas
  })
}

export function taskEnable (taskId,isEnable) {
  return axios({
    url: `v1/admin/task/enable?taskId=${taskId}&isEnable=${isEnable}`,
    method: 'get'
  })
}

export function noticeAdd(title,thumb_path,content,detail) {
  return axios({
    url: `v1/admin/notice/add`,
    method: 'post',
    data: {
      title,thumb_path,content,detail
    }
  })
}

export function noticeLists (page,pageSize) {
  return axios({
    url: `v1/admin/notice/lists?page=${page}&pageSize=${pageSize}`,
    method: 'get'
  })
}

export function noticeMod (title,thumb_path,content,detail,noticeId) {
  return axios({
    url: `v1/admin/notice/mod`,
    method: 'post',
    data: {
      title,thumb_path,content,detail,noticeId
    }
  })
}

export function noticeDelete (id) {
  return axios({
    url: `v1/admin/notice/delete?noticeId=${id}`,
    method: 'get'
  })
}

export function banneList () {
  return axios({
    url: `v1/admin/banner/lists`,
    method: 'get'
  })
}


export function bannerAdd (title,img_path,type,urls) {
  return axios({
    url: `v1/admin/banner/add`,
    method: 'post',
    data: {
      title,img_path,type,urls
    }
  })
}

export function delBanner (id) {
  return axios({
    url: `v1/admin/banner/del?bannerId=${id}`,
    method: 'get'
  })
}

export function dashboard (start,end) {
  return axios({
    url: `v1/admin/dashboard?startTime=${start}&endTime=${end}`,
    method: 'get'
  })
}

export function memberOpIntegral (userId,type,num,note) {
  return axios({
    url: `v1/admin/member/opIntegral`,
    method: 'post',
    data: {
      userId,type,num,note
    }
  })
}

export function bannerMod (title,img_path,type,urls,bannerId) {
  return axios({
    url: `v1/admin/banner/mod`,
    method: 'post',
    data: {
      title,img_path,type,urls,bannerId
    }
  })
}

export function templateAdd (task_id,task_type,img_path) {
  return axios({
    url: `v1/admin/template/add`,
    method: 'post',
    data: {
      task_id,task_type,img_path
    }
  })
}

export function templateMod (task_id,task_type,img_path,templateId) {
  return axios({
    url: `v1/admin/template/mod`,
    method: 'post',
    data: {
      task_id,task_type,img_path,templateId
    }
  })
}

export function templateList () {
  return axios({
    url: `v1/admin/template/lists`,
    method: 'get'
  })
}

export function delTemplate (id) {
  return axios({
    url: `v1/admin/template/delete?templateId=${id}`,
    method: 'get'
  })
}

export function delTaskList (id) {
  return axios({
    url: `v1/admin/task/del?taskListId=${id}`,
    method: 'get'
  })
}